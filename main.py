from mutagen.mp3 import MP3 
from PIL import Image 
import imageio 
from moviepy import editor 
from pathlib import Path 
import os 
import time


'''Here,we are using the os module to get the 
current working directory and then get the audio 
file,images folder and the video folder 
(where the final video will be saved)'''

filename1 = str(str(time.time())+".mp4")

audio_path = os.path.join(os.getcwd(), "audio.mp3") 
video_path = os.path.join(os.getcwd(), "videos") 
images_path = os.path.join(os.getcwd(), "images") 
audio = MP3(audio_path) 
# To get the total duration in milliseconds 
audio_length = audio.info.length 

# Get all images from the folder 
# Create a list to store all the images 
list_of_images = [] 
for image_file in os.listdir(images_path): 
	if image_file.endswith('.png') or image_file.endswith('.jpg'): 
		image_path = os.path.join(images_path, image_file) 
		image = Image.open(image_path).resize((400, 400)) 
		list_of_images.append(image) 

duration = audio_length/len(list_of_images) 

'''Converts all images from the images list into an images.gif file 
which will be saved in the same directory.Every image will be played for duration 
seconds.(calculated in the previous step'''

imageio.mimsave('images.gif',list_of_images,fps=1/duration) 

video = editor.VideoFileClip("images.gif") 
audio = editor.AudioFileClip(audio_path) 
final_video = video.set_audio(audio) 
os.chdir(video_path) 
final_video.write_videofile(fps=60, codec="libx264", filename=filename1) 
