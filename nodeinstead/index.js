var ffmpeg = require('fluent-ffmpeg');
ffmpeg()
  .addInput('./im.png') //your video file input path
  .addInput('./audio.mp3') //your audio file input path
  .output('./video.mp4') //your output path
  .outputOptions(['-map 0:v', '-map 1:a', '-c:v copy', '-shortest'])
  .on('start', (command) => {
    console.log('TCL: command -> command', command)
  })
  .on('error', (error) => console.log("errrrr",error))
  .on('end',()=>console.log("Completed"))
  .run()  